package sbu.cs;

public class ExerciseLecture4 {

    /*
     * implement a function that returns factorial of given n lecture 4 page 15
     */
    public long factorial(int n) {
        long result = 1;
        if (n == 0 || n == 1)
            return result;
        for (int count = 2; count <= n; count++) {
            result *= count;
        }
        return result;
    }

    /*
     * implement a function that return nth number of fibonacci series the series ->
     * 1, 1, 2, 3, 5, 8, ... lecture 4 page 19
     */
    public long fibonacci(int n) {
        // indices start from 1
        // if the index is 1 or 2, return 1
        if (n == 1 || n == 2)
            return 1;
        // return the sum of the two previous indices
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /*
     * implement a function that return reverse of a given word lecture 4 page 19
     */
    public String reverse(String word) {
        // define a new object of type StringBuilder
        StringBuilder str = new StringBuilder(word);
        // return the reversed string
        return str.reverse().toString();
    }

    /*
     * implement a function that returns true if the given line is palindrome and
     * false if it is not palindrome. palindrome is like 'wow', 'never odd or even',
     * 'Wow' lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        //convert the string to lower case letters
        String str = line.toLowerCase();
        //remove all the spaces
        str = str.replaceAll("\\s", "");
        //initialize the counters
        int i = 0, j = str.length() - 1;
        //check if there is a mismatch 
        while (i < j) {
            if (str.charAt(i) != str.charAt(j))
                return false;
            i++;
            j--;
        }
        //there is no mismatch
        return true;
    }

    /*
     * implement a function which computes the dot plot of 2 given string. dot plot
     * of hello and ali is: h e l l o h * e * l * * l * * o * lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        //the row and column of the array
        int firstLength = str1.length();
        int secondLength = str2.length();
        //initialize an empty array
        char[][] array = new char[firstLength][secondLength];
        //check each for matches and possibly place asterisks at the appropriate indices
        for(int row = 0; row < firstLength; row++){
            for(int column = 0; column < secondLength; column++){
                if(str1.charAt(row) == str2.charAt(column))
                    array[row][column] = '*';
                else
                    array[row][column] = ' ';
            }
        }
        //return the array
        return array;
    }
}
