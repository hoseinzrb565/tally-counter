package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14s
     */
    public String weakPassword(int length) {
        Random random = new Random();
        String password = "";
        for (int index = 0; index < length; index++) {
            char character = (char) ('a' + random.nextInt(26));
            password += character;
        }
        return password;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        //throw the exception if length is less than 3
        if (length < 3) {
            throw new Exception();
        }
        Random random = new Random();

        /*choosing a random special character*/

        //initialize a string of all special characters
        final String special = "\"!#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~";
        final int specialLength = special.length();
        //choose a random special character
        int randomSpecialIndex = (random.nextInt(specialLength));
        char randomSpecialCharacter = special.charAt(randomSpecialIndex);

        /*choosing a random number*/

        int randomNumber = (random.nextInt(10));

        /*choosing a random letter*/

        char randomLetter = (char) (random.nextInt(26) + 'a');

        //initialize the password string
        String password = "" + randomNumber + randomSpecialCharacter + randomLetter;

        //initialize a stringBuilder of all possible characters
        StringBuilder possibleCharacters = new StringBuilder();

        //append the special characters to the stringbuilder
        possibleCharacters.append(special);

        //append the numbers to the stringBuilder
        for (int number = 0; number < 10; number++) {
            possibleCharacters.append(number);
        }
        //append the letters to the stringBuilder
        for (int letter = 0; letter < 26; letter++) {
            //append lowercase letters
            possibleCharacters.append((char) ('a' + letter));
            //append uppercase letters
            possibleCharacters.append((char) ('A' + letter));
        }

        //fill the rest of the string(apart from the special character
        //,the number and the letter) at the beginning
        for (int index = 3; index < length; index++) {
            //choose a random possible character
            int randomIndex = random.nextInt(possibleCharacters.length());
            char randomValidCharacter = possibleCharacters.charAt(randomIndex);
            //concatenate the random character with the password string
            password += randomValidCharacter;
        }

        /*shuffle the string*/

        //convert the string to a character array
        char[] c = password.toCharArray();

        for (int index = 0; index < length; index++) {
            //generate a random index and swap the character at the current index
            //with the character at the random index
            int randomIndex = random.nextInt(length);
            char temp = c[index];
            c[index] = c[randomIndex];
            c[randomIndex] = temp;
        }

        //convert the shuffled array into a string and return the string
        return new String(c);
    }


    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public static boolean isFiboBin(int n) {

        //initialize the fibobin variable
        int i = 1;
        int fibobin = fibonacci(i) + bin(fibonacci(i));
        while (fibobin <= n) {
            if (fibobin == n) {
                return true;
            }
            i++;
            fibobin = fibonacci(i) + bin(fibonacci(i));
        }
        return false;
    }

    //fibonacci method
    public static int fibonacci(int input) {
        // indices start from 1
        // if the input is 1 or 2, return 1
        if (input == 1 || input == 2)
            return 1;
        // return the sum of the two previous indices
        return fibonacci(input - 1) + fibonacci(input - 2);
    }

    //convert the input to binary format and return the number of ones
    public static int bin(int input) {
        //convert the number to binary format
        String binaryStr = Integer.toBinaryString(input);

        //number of ones
        int count = 0;

        //iterate through the array and count the number of ones

        for (int index = 0; index < binaryStr.length(); index++) {
            if (binaryStr.charAt(index) == '1')
                count++;
        }

        return count;
    }
}


