package sbu.cs;

import java.util.*;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long total = 0;
        //iterate through the even indices and calculate the sum
        for (int index = 0; index < arr.length; index += 2) {
            total += arr[index];
        }
        return total;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reversedArr = null;

        for (int index = 0; index < arr.length / 2; index++) {
            int temp = arr[index];
            arr[index] = arr[arr.length - index - 1];
            arr[arr.length - index - 1] = temp;
        }

        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int row1 = m1.length;
        int column1 = m1[0].length;
        int row2 = m2.length;
        int column2 = m2[0].length;
        if (column1 != row2)
            throw new RuntimeException();

        double[][] product = new double[row1][column2];
        for (int i = 0; i < row1; i++) {
            for (int j = 0; j < column2; j++) {
                for (int k = 0; k < row2; k++) {
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> lists = new ArrayList<>();
        for (String[] index : names) {
            lists.add(Arrays.asList(index));
        }
        ;
        return lists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> factors = new ArrayList<Integer>();
        //add the 2's
        if (n % 2 == 0) {
            factors.add(2);
            while (n % 2 == 0) {
                n /= 2;
            }
        }

        //n is now odd
        //so we just need to check the odd numbers
        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            // i is a factor of n
            if (n % i == 0) {
                factors.add(i);
                while (n % i == 0) {
                    n /= i;
                }
            }
        }

        //if n is prime and is greater than two
        if (n > 2)
            factors.add(n);


        return factors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String line1 = line.replaceAll("[^a-zA-Z\\s]", "");
        String[] words = line1.split(" ");
        List<String> list = Arrays.asList(words);
        return list;
    }
}
